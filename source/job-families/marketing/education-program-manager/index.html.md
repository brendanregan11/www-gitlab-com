---
layout: job_family_page
title: "Education program manager"
---

As the Education Program Manager, you will be responsible for bringing GitLab into the classroom: increase the usage of GitLab in academia to enable students to take that experience to their future workplaces.


## Responsibilities

- Run and develop the [GitLab for Education](https://about.gitlab.com/solutions/education) program to grow the number of educational institutions that adopt GitLab.
- Establish relationships with educational institutions to produce inspirational case studies of their use of GitLab. Work with them to integrate GitLab in their curriculum.
- Expand the GitLab for Education program with a learning package to facilitate and incentivize the use of GitLab for educational purposes.
- Design and implement a Campus ambassador program: enable institutions and students to organize events and create materials to evangelize GitLab.

## Requirements

- You have 5-7 years of experience running developer relations or community advocacy programs, preferably open source in nature.
- Analytical and data driven in your approach to building and nurturing communities.
- You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
- Excellent spoken and written English.
- Familiarity with developer tools, Git, Continuous Integration, Containers, and Kubernetes.
- A background and relationships in the academia and research spaces are a plus.
- Ability to use GitLab
- You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.

## Key Performance Indicators

A [Key Performance Indicator](/handbook/ceo/kpis/) for this role is the number of educational institutions and students adopting GitLab per month.
