---
layout: markdown_page
title: "Usecase: GitOps"
noindex: true
---

<!--

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
-->

#### Who to contact

| Product Marketing | Technical Marketing |
| ---- | --- |
| @williamchia | @csaavedra1 |

## GitOps and Infrastructure as Code

It’s becoming increasingly crucial to automate your infrastructure as applications can be deployed to production hundreds of times per day. In addition, infrastructure is fleeting - it can be provisioned or de-provisioned in response to load.

> GitOps allows you to manage your IT infrastructure using version controlled configuration files that can generate an identical infrastructure environment whenever there is a change to the source configuration files.

In other words, GitOps allows defining infrastructure using the same principles that Development teams use for source code. Similar to how source code for applications generates the same application binaries every single time, GitOps helps generate the same infrastructure environment every time it is deployed.

GitOps brings in **DevOps best practices to infrastructure**, which includes:
* *Versioning* - A single source of truth for infrastructure definition and changes. Infrastructure as Code enables all configurations and policies to be stored as code (e.g., Auto Scaling / Rollout policies). The actual desired state may or may not be not stored as code (e.g., number of replicas, pods).
* *Manage change* - The merge request is where changes are tracked for retrospect and audit purposes, where code quality is ensured, where teams collaborate via reviews and comments
* *Continual convergence* - Continuously converge to the desired state defined in Git using Git Workflows and CI/CD that triggers a release pipeline to configure the target environment
* *Observe* - Configure monitoring and alerts as code, track performance of the target environment, alert on infrastructure issues and offer feedback loop into GitOps

GitOps offers benefits such as:
* *Less error prone* - infrastructure definition is codified, hence repeatable and less prone to human error
* *Faster time to market* - execution via code is faster than manual point and click, test cases can be automated and hence repeatable in a consistent manner to deliver stable environments rapidly, at scale
* *Less risk* - all changes to infrastructure are tracked, leaving traceability for audit and ability to rollback to a previous desired state with ease
* *Reduce costs* - automation of infrastructure definition and testing eliminates manual tasks and rework improving productivity, reduced downtimes due to built in revert/rollback capability
* *Enhanced Developer Experience* - developers work with code and not with unfamiliar containers / infrastructure

GitOps is a more prescriptive workflow of Infrastructure as Code. GitOps with GitLab helps you manage physical, virtual and cloud native infrastructures (including Kubernetes and Serverless technologies) with a tight integration with industry leading Infrastructure Automation tools like Terraform.

## Personas

### User Persona
Infrastructure as Code requires understanding of the platform and the desired state of the application environment. Users of Infrastructure as Code have a good understanding of both Git as a SCM tool as well as the platform they are expected to provision and manage. Below are a few power users Infrastructure as Code:

* [Sydney, the System Administrator](/marketing/product-marketing/roles-personas/#sidney-systems-administrator)
  Sydney defines, maintains and scales the infrastructure and configuration for the application teams. She often receives repetitive requests on the same task. Sydney's primary motivation is to automate repetitive tasks to minimize errors and save time as well as define the infrastructure and configuration in a way that changes are tracked and to stop infrastructure changes becoming the wild west.

* [Devon, the DevOps Engineer](/marketing/product-marketing/roles-personas/#devon-devops-engineer)
  Devon is often the Ops interface for the development team. He provides support for infrastructure, environments and integrations. Devon is fairly conversant with code and would prefer administering infrastructure via code rather than a multitude of different tools and context switches.

* [Priyanka, the Platform Operator](/marketing/product-marketing/roles-personas/#priyanka-platform-operator)
  Infrastructure management is one of the main responsibility of the platform team. Priyanka is responsible for providing, maintaining and operating a shared platform - either traditional or modern cloud platform  - which the development teams utilize to ship and operate software more quickly.

### Buyer Personas
Buyers of Infrastructure as Code are usually leaders who lead infrastructure / automation initiatives. Typical buyer personas are:
* **CIO / Vice President of IT** - experience in planning, design and execution of digital transformation programs, implementing new operating models. Typically leads both engineering and operations teams
* **Vice President of IT Infrastructure** (also referred to as IT Operations in some organizations) - experience in planning, design and execution of IT infrastructure services - including deploying and managing cloud services, system management and service desk. Frequently has the agenda of reducing IT costs for the organization.
* **Vice President of Platform Engineering** - Managing a shared platform for development teams is one of the main agendas of the Platform Engineering team. The platform team has expertise in new technologies like Kubernetes. Key KPIs of the platform engineering team are Automation, Efficiency and Self Service.

## Analyst Coverage

List key analyst coverage of this usecase


## Market Requirements

| Market Requirement |  Description  |  Typical features  |  Value |
|----------|-------------|----------------|-------|
| abc  |  def  |  ghi  |  jkl  |  

## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
|  abc  | def | ghi  |


## [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

### Discovery Questions
- list key discovery questions


## Competitive Comparison
TBD - will be a comparison grid leveraging the capabilities

### Industry Analyst Relations (IAR) Plan
- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=1124037301).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.

## Proof Points - customers

### Quotes and reviews
- [Video: kiwi.com on Infrastructure as Code](https://www.youtube.com/watch?v=Un2mJrRFSm4) - Learn how kiwi.com uses GitLab and Terraform to manage their infrastructure as code. Bonus info - see how they deploy self hosted gitlab instance from gitlab CI/CD and Infrastructure as Code
- [Video: VMware - Infrastructure as Code with GitLab and Terraform Cloud](https://www.youtube.com/watch?v=qXj4ShQZ4IM) - Gitlab and Terraform have worked well together for some time; Learn how VMware uses GitLab CI/CD and Terraform Cloud to manage infrastructure as code

### Case Studies

-**[Northwestern Mutual](https://www.youtube.com/watch?v=yw7N82mXmZU)**
* **Problem**  Manual processes, provisioning of resources from On-Prem Datacenters created inefficient practices. 
* **Solution** GitLab Premium (SCM,CI) and Terraform
* **Result** Drastically improved lead time to production, Environments can be created in under an hour. Everything is done as code and their are no risks from patching. 
* **Sales Segment:** Enterprise

### References to help you close
- Link to SFDC list of use case specific references

## Partners
GitLab is not a replacement for existing Infrastructure Automation tools, but rather complements them to provide a comprehensive solution. As per the [JetBrains DevOps Ecosystem 2019](https://www.jetbrains.com/lp/devecosystem-2019/devops/) survey, **Terraform** is the most popular infrastructure provisioning tool used by customers. Terraform is cloud-agnostic and helps manage complex infrastructures for distributed applications. GitLab will [focus on Terraform support](/direction/configure/infrastructure_as_code/#whats-next) as the first step towards building a comprehensive GitOps solution.

## Resources
### Presentations
* LINK

### Whitepapers, infographics, blogs
* [Infrastructure as Code using GitLab & Ansible](/blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/)
* [Part 1 of 3: Why collaboration technology is critical for GitOps](/blog/2019/11/04/gitlab-for-gitops-prt-1/)
* [Part 2 of 3: How infrastructure teams use GitLab and Terraform for GitOps](/blog/2019/11/12/gitops-part-2/)
* [Part 3 of 3: How to deploy to any cloud using GitLab for GitOps](/blog/2019/11/18/gitops-prt-3/)

### Videos (including basic demo videos)
* [What is Infrastructure as Code](https://www.youtube.com/watch?v=zWw2wuiKd5o)
* [Infrastructure as Code using GitLab & Ansible](https://youtu.be/M-SgRTKSeOg)
* [Part 1 of 3: How GitLab supports GitOps: The Process](https://www.youtube.com/watch?v=wk7YAXijIZI)
* [Part 2 of 3: How GitLab supports GitOps: The Infrastructure](https://www.youtube.com/watch?v=5rqoLj8N5PA)
* [Part 3 of 3: How GitLab supports GitOps: The Application](https://www.youtube.com/watch?v=heQ1WY_08Tc)
* [GitOps with GitLab and Terraform](https://www.youtube.com/watch?v=G7JOjI6V3AY)
* [Using GitLab for GitOps to break down silos and encourage collaboration](https://www.youtube.com/watch?v=5ykRuaZvY-E)

### Clickthrough & Live Demos
* [GitOps Click Through Demo](https://drive.google.com/open?id=1UT32lLvXtwAslkK7o8asbko3a231WKrjmlcM0z9coPw)
