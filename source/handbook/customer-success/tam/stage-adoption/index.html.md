---
layout: handbook-page-toc
title: "Stage Adoption"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## Stage Adoption

As part of the TAM's mandate to drive stage adoption with customers, we need to define exactly what it means to adopt a stage at GitLab.  
This page is to define what it takes to say a customer has adopted that stage within GitLab.

* **Manage** - using 2 of the following...
   * Making use of [Audit Events](https://docs.gitlab.com/ee/administration/audit_events.html) at the instance level as part of managing GitLab.
   * Using [Code Analytics](https://about.gitlab.com/direction/manage/code-analytics/) and [Insights](https://docs.gitlab.com/ee/user/group/insights/) within GitLab.
   * Using [Compliance Management](https://about.gitlab.com/direction/manage/compliance-management/) within GitLab.
   * Using [Value Stream Management](https://about.gitlab.com/solutions/value-stream-management/) within GitLab.
* **Plan**
   * Using Issue Tracking and/or Epics across 50% of teams.
* **Create**
   * Using GitLab for git repo storage and code review (MR's).
* **Verify**
   * Have made instance level shared Runner(s) available.
* **Package**
   * Using one or more of our registries (Package Registry, Container Registry, Helm Registry).
* **Secure**
   * Using one of more of our security testing tools (SAST, DAST, Container Scanning, Dependency Scanning)
* **Release**
   * Using GitLab CI/CD to deploy their product.
   * From a feature standpoint, if they are using 2 of the following features...
      * [Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/)
      * [Review Apps](https://about.gitlab.com/stages-devops-lifecycle/review-apps/)
      * [Feature Flags](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html)
      * [Release Orchestration](https://docs.gitlab.com/ee/user/project/releases/)
* **Configure**
   * Using AutoDevOps, Kubernetes or Infrastructure As Code with 50% of teams.
* **Monitor**
   * Using Prometheus & Grafana to monitor their GitLab server.
   * Or using Prometheus to monitor 50% of their project deployments.
* **Defend**
   * Using the [Web Application Firewall](https://docs.gitlab.com/ee/user/clusters/applications.html#web-application-firewall-modsecurity) within GitLab.