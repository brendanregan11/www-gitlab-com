---
layout: handbook-page-toc
title: "Customer Health Assessment and Triage Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----

# Customer Health Assessment and Account Triaging

This page covers the factors to consider for customer health, guidelines for selecting the appropriate rating, communication guidelines, and instructions for the account triage issue creation.

## Customer Health Assessment

The following guideline will provide Technical Account Managers (TAMs) guidance to choose the right health assessment for her/his customer account. Health considers both delivery of value and outcomes to customers and business impact to GitLab.

### Risk Factors to Consider

The following are factors when considering product health, noting specific examples of things that could bring risk to customer health.

- **Product adoption**: There is a delayed, low or materially reduced usage (i.e., drop in usage) as measured by license consumption, features / use cases, product version (self-managed only), and/or GitLab stages. Value and outcome delivery to the customer misses expectations.
- **Product experience**: Customer has enhancements or defect fixes that are necessary for a customer and measured by the criticality of the request, severity of the issues and/or number of enhancements and defects. Missed expectations for feature release can also impact product experience.
- **Lack of customer engagement**: Customer contact(s) are not responsive, miss meetings and/or unwilling to engage in cadence calls or other engagements like [EBRs](.../customer-success/tam/ebr/).
- **Loss of executive sponsor or champion**: Sponsor or champion leaves the company, moves to a different part of the organization and/or has reduced scope of influence
- **Low customer sentiment**:  The customer has expressed concerns and/or dissatisfaction with their experiences with GitLab (i.e., sales, professional services, support, product, etc.) through direct conversations, surveys (e.g., NPS), social media or other communication channels. 
- **Other organizational factors**: The customer's business performance is materially impacted and declining. The company is acquired, merging with another company, divested or another structural change to customer's business. 

### Health Rating 

The factors above (i.e., value delivery to customer, product adoption and experiences, customer engagement and sentiment) should be considered to determine the appropriate health rating. The TAM makes this determination based on her/his customer engagement and communications as well as metrics like adoption telemetry and NPS and other sources of information (e.g., news, social media, blogs, etc.).

- **Green**: Engagement, adoption and experiences are as expected or better than expected
- **Yellow**: Engagement, adoption and/or experiences are lower than expected, risking:
  - GitLab's ability to deliver customer value and outcomes and/or
  - Potentially impacting the renewal and/or future revenue growth.
  - This can be a number of risk factors or only one.
- **Red**: Engagement, adoption and/or experiences are significantly lower than expected:
  - Materially impacting or blocking GitLab's ability to delivery customer value and outcomes and/or
  - Bringing direct high risk to renewal and/or future revenue growth.
  - If there is risk to customer churn (i.e., loss of customer), the rating should be red.
  - This can be a number of risk factors or only one with high priority or criticality.

### Communication Guidelines

The following are guidelines on who to notify when an account is yellow or red. Please make sure the following people are notified with the respective customer health ratings.

#### Yellow Health Rating

- Account Team (i.e., Strategic Account Leader or Account Executive, Solution Architect)

- Regional TAM Manager
- TAM Director (all non-Public Sector customers) or Director of Customer Success Public Sector (for Public Sector customers)

#### Red Health Rating

- Include the list above as well as...
- Area Sales Manager and Regional Director
- Vice President of Customer Success

## Account Triage Project

An account risk issue should be created in the [Account Triage Project](https://gitlab.com/gitlab-com/customer-success/account-triage) if the customer health assessment is either yellow or red. These are also viewable in the [TAM Risk Account Issue Board.](https://gitlab.com/gitlab-com/customer-success/account-triage/-/boards/703769) 

### Responsibilities

The TAM is responsible for coordinating with all relevant parties to develop a plan to address the risks. Typically, this will involve the account team and communication group (above), as well as other resources such as Product Managers, marketing, executive or engineering resources meeting to develop and deliver the plan to address the risks. The TAM then drives execution of the strategy and is responsible for regular updates to the triage issue. When the risks have been addressed bringing the customer to a healthy / green status, the triage issue can be closed.

### Issue Template

After creating an issue in the [Account Triage](https://gitlab.com/gitlab-com/customer-success/account-triage) project, it will be created with a default template requiring the following information for the description:

**Salesforce URL**

- Enter the URL of the customer's Salesforce record.

**Contract Renewal Date**

- Include the date for which the customer contract is set to next renew with GitLab based on the Salesforce record.

**Reason for Health Score**

- Using the Health Scoring Criteria, provide justification for why the account is in an Amber or Red state.

The milestone for the issue should be based on the critical timing the issue, enhancement or requests need to be resolved (e.g., customer-defined deadline, renewal date, etc.).

### Issue Classification Labels

~E&A Expected and Avoidable
~E&U Expected and Unavoidable
~U&A Unexpected and Avoidable
~U&U Unexpected and Unavoidable

### Risk Labels

~HS::Green  Green Health Rating 
~HS::Yellow  Yellow Health Rating 
~HS::Red  Red Health Rating 

### Region Labels

- ~US-WEST
- ~US-EAST
- ~EMEA
- ~APAC
- ~LATAM

## Related Processes

[Customer Success Escalations Process](https://about.gitlab.com/handbook/customer-success/tam/escalations/)

