---
layout: handbook-page-toc
title: "Solutions Architects"
---

# Solutions Architect Handbook
{:.no_toc}

Solutions Architects (SA) are the trusted advisors to GitLab prospects and customers during the presales motion, demonstrating how the GitLab application and GitLab Professional Services address common and unique business requirements. SA's add value to GitLab by providing subject matter expertise and industry experience throughout the sales process.

SA's identify and propose comprehensive solutions to complex business problems and workflow scenarios after compiling inputs from customer conversations. Desired inputs include pain points, role-based visibility concerns, opportunities for improved efficiencies, current technologies and tools, corporate initiatives, target outcomes and more. SA's also act as the technical liaison between the sales team and other groups within GitLab, engaging GitLab employees from other teams as needed in order meet the needs of the customer.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Role & Responsibilities

See the [Solutions Architect role description](/job-families/sales/solutions-architect/)

In addition to core responsibilities, Solutions Architects may assist in other client-facing activities aligned to Content Marketing or Strategic Marketing such as blogs, videos, webinars, presentations and industry trade show presence. Some examples are listed below:

* GitLab Security Overview [Video](https://www.youtube.com/watch?v=SP0VSH-NqJs)
* GitLab High Availability and Geo Overview [Video](https://www.youtube.com/watch?v=fji7nvmOHNQ)
* GitLab GitHub Migration [Video](https://www.youtube.com/watch?v=VYOXuOg9tQI)
* GitLab Source Code Management [Video](https://www.youtube.com/watch?v=P6jD966jzlk)
* GitLab Blog [Contribution](/blog/2018/02/20/whats-wrong-with-devops/)
* Gitlab with Rancher and EKS [Video](https://www.youtube.com/watch?v=kUwHBIFXciY)

## Demo Readiness

Solutions Architects are occasionally called on at a moments notice to give a demo or join a call to show a prospect or customer specific GitLab functionality. A best practice for being prepared for these demo requests is to have a minimized browser window with various content preloaded into the browser tabs. Examples of common environments utilized are listed below. Note that accessing some of these links are only enabled for GitLab team-members.

* The [GitLab Demo Systems](/handbook/customer-success/demo-systems/) provides a catalog of demos and the sandbox infrastructure for performing demos.
* The [GitLab.org Group level](https://gitlab.com/groups/gitlab-org/-/roadmap) clearly represents GitLab Epics, Roadmaps and cross-project issue views used for planning releases
* A [GitLab.com project](https://gitlab.com/jkrooswyk/joel-springsample/boards/579466) driven by Auto DevOps which includes a populated issue board as well as an active Merge Request is highly useful for describing value of workflow utilizing GitLab Auto DevOps for build, test, security and review
* A privately hosted [project](https://gitlab.demo.i2p.online/jkrooswyk/The-Monolith/environments) which utilizes a custom CI pipeline as well as more complex deploy environments and standard monitoring is useful for custom CI discussions and visualizing more complex environment requirements
* This [GitLab.com project](https://gitlab.com/gitlab-examples/security/security-reports/merge_requests/2) is commonly utilized specifically for security discussions
* The [production monitoring for GitLab.com](https://gitlab.com/charts/gitlab/environments/190276/metrics) is useful for any discussion regarding custom and in-app monitoring
* The [production Grafana instance](https://dashboards.gitlab.com/d/000000159/ci?refresh=5m&orgId=1) showing GitLab Runner details is a great way to represent the art of the possible as it relates to monitoring and dashboards
* The GitLab [Direction](/direction) page in the Handbook is often beneficial for future-looking product vision questions and discussions about upcoming features
* A group of [golden repositories](https://gitlab.com/gitlab-com/customer-success/demos/golden-repos) can be leveraged to show prospects repositories and pipelines more relevant to their programming languages and deploy environments
* Optional: [Integrations](/handbook/marketing/product-marketing/demo/integrations) may be loaded and ready to discuss by leveraging the standard demo environments

## Recommended Tools for Customer-Facing Meetings

Solutions Architects frequently interact with customers for demos, presentations or Q&A. These calls should enable the customer to clearly experience the value of GitLab without distraction or interruption. The below list of tools was compiled by the GitLab SA team as commonly used solutions. Note, this list does not represent a requirement to use any of these products or an endorsement for these products.

* [Muzzle](https://muzzleapp.com/) to mute all notifications prior to beginning a call
* [Tab Resize Chrome plugin](https://chrome.google.com/webstore/detail/tab-resize-split-screen-l/bkpenclhmiealbebdopglffmfdiilejc?hl=en-US) to break tabs into split-screen viewing
* [Screenbrush](http://screenbrush.imagestudiopro.com/) to draw on the screen
* [Toby](http://www.gettoby.com/) or [Tabs Outliner](https://chrome.google.com/webstore/detail/tabs-outliner/eggkanocgddhmamlbiijnphhppkpkmkl) to launch many preset tabs at once
* [Station](https://getstation.com/) to group pages by application in a smart dock
* [MouseBeam](https://geeky.gent/tag/mousebeam/) enables the mouse cursor to use multiple screens like a circle
* [~~Spectacle~~](https://www.spectacleapp.com/) to quickly move and resize windows [Spectale DEPRECATED October 2019](https://github.com/eczarny/spectacle#important-note)
* [Dark Reader](https://darkreader.org/) enables browser dark mode to better fit room lighting
* [Postman](https://www.getpostman.com/) for API interaction

Related macOS tips
* [Switch between full screen applications](https://www.intego.com/mac-security-blog/how-to-enter-and-exit-full-screen-mode-in-macos/) using the trackpad, Command keys or other options
* Use the [Zoom Accessibility Features](https://www.imore.com/how-use-zoom-mac) to zoom in on targeted screen locations

## Working Agreements

Enterprise Solutions Architects typically support two sales teams made up of Sales Development Representatives, Strategic Account Leaders and Technical Account Managers. Commercial Sales Solutions Architects support Mid-Market Account Executives and SMB Customer Advocates in a pooled model. When joining a sales team, establishing working agreements is critical to providing optimal service to the customers as well as the GitLab team. A sample template of working agreements is found below to help facilitate conversation and establish these agreements:  

1. **Customer response time for emails and meeting followups** I will always do my best to provide same-day responses to customer inquiries and follow ups unless otherwise noted. I like to provide customers top-notch service, but interruptions can affect that target. I will use my out of office when traveling so customers can expect delayed responses during those times. Feel free to contact me if it's approaching the end of the day and you didn't see me address a customer request. Slack is the easiest way to find me most of the time.
1. **Calendar** Schedule things as necessary right on my calendar during working hours - I'll do my best to keep my calendar up to date. However, if you see only one empty slot for a day or if it looks like the meeting would be back to back with something else, please check with me before confirming dates and times with the customer. If you want me at my best for you, I will need time to prepare and followup. If we schedule back to back, I've found I'm often late to arrive due to the last meeting, flustered as I context shift, lacking answers for known questions, unprepared with demo environments that were altered in the last call, etc.
1. **Regular communication** A weekly strategy hour-long call with the SA, SAL, SDR and TAM is preferred. If more frequent synchronous communication is desirable or required, we should work together as a team to identify a viable cadence, required attendees, convenient time and agenda for those calls.
1. **Elevated conversations** The more we focus on value and stay away from very deep technical and troubleshooting conversations, the more I can help you achieve target sales. I'm happy to review and contribute to business plans so we remain in sync.
1. **Professional Services** I'm happy to recommend and propose services for prospects and customers. I will write the initial SOW and get approvals from the Professional Services team.
1. **Travel** While strategic enterprise sales absolutely require travel, each day on the road can increase the volume of work and calls on other days of the week as my dialogs with customers often require extensive time commitments to research and model specific customer solutions. Please plan strategically when looking at on-sites, considering that I support at least two sales teams. Additionally, please do what you can to avoid scheduling on-site visits less than one week out to help me be able to commit to family and local things.
1. **Notes** Unless otherwise directed, call notes will be linked to a Google document from Salesforce. Email communication with customers will also be bcc'd to Salesforce. If you prefer to record notes and actions differently, please let me know what that collaborative method is.
1. **Continuous learning** Things at GitLab move really fast, and I need time to keep up. I'll block a few hours on my calendar coincident with releases to absorb the latest GitLab feature set, but new technologies and competitive products also appear regularly. Please expect that I will need to allocate additional time to learning about those on a regular basis to sufficiently support our customers.

## Engaging a Solutions Architect

#### Qualification

1. What is the reason to engage the SA team?
  * Discovery after lead qualification is complete
  * GitLab demos
  * GitLab technical deep dives
  * Customer GitLab days
  * Whiteboarding sessions
  * Customer questions
  * RFI/RFP/Security audits
  * Marketing event support
  * Statement of Work for Professional Services

1. What is the opportunity?
  * Share any existing notes about the opportunity
  * Key players and personas
  * Other relevant information shaping an engagement
  * Is there a business plan or sales approach defined?

#### Preparation

1. **Outcome:**  
  * What’s in it for the client?  
  * Why look at a new strategy for software development?
  * Solutions Architects need to be able to tailor conversations and demos to demonstrate value as well as how to address current problem areas.

1. **Infrastructure:**
  * What tools are currently in place?  
  * What tools have potential to be replaced?
  * What processes or workflows have potential to change?

2. **Challenges:**
  * What problems/roadblocks have been uncovered?
  * What is the current release velocity?
  * What is the current planning process?
  * What visibility, traceability or efficiency concerns exist?
  * What collaboration or governance opportunities exist?
  * What security or compliance inefficiencies exist?

#### SA/TAM engagement overlap

The SA owns all pre-sales technical relationships and activities. The SA quarterbacks technical conversations prior to the sale. TAM involvement prior to the sale models the expectations for customer relationships after the sale. TAM involvement should supplement, not displace, SA pre-sales ownership of the account.  

TAM engagement prior to the sale should occur in the following situations:  

* During POV kickoff or when appropriate in deep conversations on technology if no POV will occur
* When a shared Slack channel is created for the customer
* A shared customer issue tracking project has been created that will affect the account long-term
* As requested by the SA if the TAM has a specific subject matter expertise relevant to the conversation

The TAM owns the account post-sale. SA's are to remain involved in the following situations:  

* Development of expansion plans with the SAL and TAM
* A new POV or product evaluation begins for a single team or an enterprise-wide upgrade - anything affecting IACV may require SA involvement
* Any product license expansions that require overviews, education and competitive insights prior to close
* Any professional services are being discussed and an SOW may required drafting
* If a TAM is over-committed or unable to support a customer request, the SA may be requested to assist
* Support of GitLab days or other on-site evangelism of GitLab at customer sites

## When and How to Engage a Solutions Architect

The SA should either be included in a discovery call or provided with Outcome / Infrastructure / Challenges  information uncovered by prior interactions with the account.  Occasionally, SA's may support a combination call of discovery and technical demonstration/deep-dive on a single call, but this is suboptimal as the latter approach does not allow the SA time to prepare for and/or tailor the discussion.

The SA is also responsible for any pre-sales technical customer inquiry or audit from associated accounts, including RFI, RFP or security audits. For details on the audit process, proceed to the [Security](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html#how-to-request-a-questionnaire-be-completed) page.

The SA is always responsible for drafting Professional Services SOW's, regardless of the account status (pre- and post-sales).

As a general guideline, no more than 2 Solutions Architects should join any single client-facing call, whether it be assistive for the topic or for learning / shadowing.

### Engaging a Solutions Architect for Marketing Events

When technical staff is required for a marketing event, follow the process outlined on the [Marketing Events](/handbook/marketing/events/#requesting-technical-staffing) page. SA managers and others in Customer Success should monitor the [Event Staffing Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1254947) regularly for new events (although Marketing staff will CC the appropriate managers on issues.)

### U.S. Strategic Account Engagement Model

U.S. Enterprise SA's are aligned regionally with Strategic Account Leaders and aligned in their respective account activities.  Each team collaborates according to their own Working Agreements.

When workload exceeds the SA's capacity or when there is a request from other departments, please submit the request for assistance on either of the enterprise triage boards based on region: [US EAST Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/us-east-enterprise-triage/boards) or [US WEST Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/us-west-enterprise-triage/boards). The issue creation and triage processes are described in detail below.

### EMEA Account Engagement Model

SA engagement for customer interactions, RFP's, audits and more can be requested by any EMEA-based sales executive or other GitLab team-member using the appropriate GitLab issue board. The [EMEA Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/emea-triage/boards) is leveraged by the team in order to ensure coverage for all sales activity.  The issue creation and triage processes are described in detail below.

### Commercial Engagement Model

SA engagement for customer interactions, RFP's, audits and more can be requested by an SMB or Mid-Market Account Executive or other GitLab team-member using the appropriate GitLab issue board. Commercial boards exist for both the Americas and EMEA. The [Americas Commercial Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/boards/1006966) and the [EMEA Commerical Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/emea-commercial-triage/-/boards) are leveraged by the team in order to ensure coverage for all sales activity.  The issue creation and triage processes are described in detail below.

### APAC Account Engagement Model

Customer Success Team (SA/TAM) engagement for customer interactions, RFP's, audits and more can be requested by a Strategic Account Leaders, an SMB or Mid-Market Account Executive or other GitLab team-member using the appropriate GitLab issue board. A single APAC Triage board exists that serves both Enterprise and Commerical markets. The [APAC  Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/apac-triage/-/boards/1354580) is leveraged by the team in order to ensure coverage for all sales activity.  The issue creation and triage processes are described in detail below.


### Issue Creation Details

1) Navigate to the correct board for your region. NOTE: All triage boards are located in the [SA Triage Boards](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards) group in projects broken out by region or engagement model.

2) Create a new issue.

3) Use one of the available templates:

*  New activity: use the "SA Activity" template to ensure the proper data is collected and available
*  Follow up activity: use the "Follow Up" template
*  Security audit: use the "Vendor Security Assessment" template

4) To help the SA group ensure success for the client, the following information should be made available *prior* to SA engagement

* **Customer Information**: Customer name, SFDC opportunitylink
* **Preferred Date Options**: When would the customer want to have the call (including time zone information)
* **Target Hosting Environment**: Self-hosted, cloud or GitLab.com?
* **Challenges/Pain Points**: What is the customer pain/problems?  
* **Required Capabilities**: What does the customer need?  
* **Current Tools/Competitors**: What is the current tool set? Are any of those tools non-negotiable?
* **SA Asks**: What do you need from the SA's (demo, technical Q&A, SOW, etc.)

5) Add one or more of the following labels as needed (labels listed below).

### Board Label Descriptions

* **Open/no label**: Newly submitted issues
* **SA Triaged**: Issue ownership has been claimed by an SA
* **SA Demo Ready**: Prep has been completed for a customer interaction and further scheduling or conversations can now occur
* **Security Audit**: This request requires security team involvement, typically upon receiving a customer security questionnaire
* **Services Request**: If this opportunity focuses on services, use this label to indicate that
* **SA Doing**: Once an SA signals they are actively working the issue, the issue is moved to 'Doing'. The issue will stay in this status until the SA, customer and sales team agree the issue has been completed
* **SA Followup**: Followup work is required by the SA for this particular request (example: questions needing research after a call ends)
* **SA Waiting**: This label indicates that an SA is waiting on the requestor for more information before advancing this issue
* **SA POV**: A Proof of Value is being requested or needs support

### Triage of Issues

The SA team associated with each region will monitor the triage board and/or an associated Slack channel for incoming work and will contact the appropriate Customer Success team members via Slack, email or phone depending on required skills, schedules and availability. SLA for activity is 48 hours unless the issue is marked with an 'Expedite' label.
